cmake_minimum_required(VERSION 3.4.1)

add_library(
             lwjgl-core
             SHARED
             src/main/jni/memoryaccessjni.c
             src/main/jni/libcstdlib.c
             src/main/jni/libcstring.c
             src/main/jni/jninativeinterface.c )

target_link_libraries( lwjgl-core )
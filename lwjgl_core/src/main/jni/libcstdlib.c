#include <jni.h>

#include <stddef.h>
#include <stdlib.h>

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCStdlib_nmalloc(JNIEnv *env, jclass type, jlong size) {

    return (jlong) malloc((size_t) size);
}


JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCStdlib_ncalloc(JNIEnv *env, jclass type, jlong nmemb,
                                                             jlong size) {

    return (jlong) calloc((size_t) nmemb, (size_t) size);
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCStdlib_nrealloc(JNIEnv *env, jclass type, jlong ptr,
                                                              jlong size) {

    return (jlong) realloc((void * ) ptr, (size_t) size);
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCStdlib_nfree(JNIEnv *env, jclass type, jlong ptr) {

    free((void * ) ptr);
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCStdlib_naligned_1alloc(JNIEnv *env, jclass type,
                                                                     jlong alignment, jlong size) {
#if __ANDROID_API__ >= 28
    return (jlong) aligned_alloc((size_t) alignment, (size_t) size);
#else
    return 0L;
#endif
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCStdlib_naligned_1free(JNIEnv *env, jclass type,
                                                                    jlong ptr) {

#if __ANDROID_API__ >= 28
    return (jlong) aligned_free(ptr);
#endif
}
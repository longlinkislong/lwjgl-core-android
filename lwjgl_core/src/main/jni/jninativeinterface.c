#include <jni.h>


JNIEXPORT jobject JNICALL
Java_com_longlinkislong_lwjgl_system_jni_JNINativeInterface_nNewDirectByteBuffer(JNIEnv *env,
                                                                                 jclass type,
                                                                                 jlong address,
                                                                                 jlong capacity) {

    return (*env)->NewDirectByteBuffer(env, (void * ) address, capacity);
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_jni_JNINativeInterface_GetDirectBufferAddress(JNIEnv *env,
                                                                                   jclass type,
                                                                                   jobject buf) {

    return (jlong) (*env)->GetDirectBufferAddress(env, buf);
}
#include <jni.h>
#include <stdint.h>

JNIEXPORT jint JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_getPointerSize(JNIEnv *env, jclass type) {
    return (jint) sizeof(void *);
}

JNIEXPORT jbyte JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_ngetByte(JNIEnv *env, jclass type, jlong ptr) {
    return ((jbyte * ) ptr)[0];
}

JNIEXPORT jshort JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_ngetShort(JNIEnv *env, jclass type,
                                                               jlong ptr) {

    return ((jshort * ) ptr)[0];
}

JNIEXPORT jint JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_ngetInt(JNIEnv *env, jclass type, jlong ptr) {

    return ((jint * ) ptr)[0];
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_ngetLong(JNIEnv *env, jclass type, jlong ptr) {

    return ((jlong *) ptr)[0];
}

JNIEXPORT jfloat JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_ngetFloat(JNIEnv *env, jclass type,
                                                               jlong ptr) {

    return ((jfloat *) ptr)[0];
}

JNIEXPORT jdouble JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_ngetDouble(JNIEnv *env, jclass type,
                                                                jlong ptr) {

    return ((jdouble *) ptr)[0];
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_ngetAddress(JNIEnv *env, jclass type,
                                                                 jlong ptr) {

    return (jlong) ((intptr_t * ) ptr)[0];
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_nputByte(JNIEnv *env, jclass type, jlong ptr,
                                                              jbyte value) {

    ((jbyte * ) ptr)[0] = value;
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_nputShort(JNIEnv *env, jclass type, jlong ptr,
                                                               jshort value) {

    ((jshort * ) ptr)[0] = value;
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_nputInt(JNIEnv *env, jclass type, jlong ptr,
                                                             jint value) {

    ((jint * ) ptr)[0] = value;
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_nputLong(JNIEnv *env, jclass type, jlong ptr,
                                                              jlong value) {

    ((jlong * ) ptr)[0] = value;
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_nputFloat(JNIEnv *env, jclass type, jlong ptr,
                                                               jfloat value) {

    ((jfloat * ) ptr)[0] = value;
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_nputDouble(JNIEnv *env, jclass type, jlong ptr,
                                                                jdouble value) {

    ((jdouble * ) ptr)[0] = value;
}

JNIEXPORT void JNICALL
Java_com_longlinkislong_lwjgl_system_MemoryAccessJNI_nputAddress(JNIEnv *env, jclass type,
                                                                 jlong ptr, jlong value) {

    ((intptr_t * ) ptr)[0] = (intptr_t) value;
}

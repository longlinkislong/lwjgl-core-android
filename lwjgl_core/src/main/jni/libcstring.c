#include <jni.h>

#include <stddef.h>
#include <string.h>


JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemset__JIJ(JNIEnv *env, jclass type,
                                                                  jlong dest, jint c, jlong count) {

    memset((void * ) dest, (int) c, (size_t) count);
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemcpy__JJJ(JNIEnv *env, jclass type,
                                                                  jlong dest, jlong src,
                                                                  jlong count) {

    return (jlong) memcpy((void * ) dest, (const void * ) src, (size_t) count);
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemmove__JJJ(JNIEnv *env, jclass type,
                                                                   jlong dest, jlong src,
                                                                   jlong count) {

    return (jlong) memmove((void * ) dest, (void * ) src, (size_t) count);
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemset__Lbyte_3_093_2IJ(JNIEnv *env,
                                                                              jclass type,
                                                                              jbyteArray dest_,
                                                                              jint c, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);

    void *result = memset(dest, (int) c, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemset__Lshort_3_093_2IJ(JNIEnv *env,
                                                                               jclass type,
                                                                               jshortArray dest_,
                                                                               jint c,
                                                                               jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);

    void *result = memset(dest, (int) c, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemset__Lint_3_093_2IJ(JNIEnv *env,
                                                                             jclass type,
                                                                             jintArray dest_,
                                                                             jint c, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);

    void *result = memset(dest, (int) c, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemset__Llong_3_093_2IJ(JNIEnv *env,
                                                                              jclass type,
                                                                              jlongArray dest_,
                                                                              jint c, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);

    void *result = memset(dest, (int) c, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemset__Lfloat_3_093_2IJ(JNIEnv *env,
                                                                               jclass type,
                                                                               jfloatArray dest_,
                                                                               jint c,
                                                                               jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);

    void *result = memset(dest, (int) c, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemset__Ldouble_3_093_2IJ(JNIEnv *env,
                                                                                jclass type,
                                                                                jdoubleArray dest_,
                                                                                jint c,
                                                                                jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);

    void *result = memset(dest, (int) c, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemcpy__Lbyte_3_093_2Lbyte_3_093_2J(
        JNIEnv *env, jclass type, jbyteArray dest_, jbyteArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memcpy(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemcpy__Lshort_3_093_2Lshort_3_093_2J(
        JNIEnv *env, jclass type, jshortArray dest_, jshortArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memcpy(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemcpy__Lint_3_093_2Lint_3_093_2J(JNIEnv *env,
                                                                                        jclass type,
                                                                                        jintArray dest_,
                                                                                        jintArray src_,
                                                                                        jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memcpy(dest, src, (size_t) count);

    (*env)->ReleaseIntArrayElements(env, dest_, dest, 0);
    (*env)->ReleaseIntArrayElements(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemcpy__Llong_3_093_2Llong_3_093_2J(
        JNIEnv *env, jclass type, jlongArray dest_, jlongArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memcpy(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemcpy__Lfloat_3_093_2Lfloat_3_093_2J(
        JNIEnv *env, jclass type, jfloatArray dest_, jfloatArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memcpy(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemcpy__Ldouble_3_093_2Ldouble_3_093_2J(
        JNIEnv *env, jclass type, jdoubleArray dest_, jdoubleArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memcpy(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemmove__Lbyte_3_093_2Lbyte_3_093_2J(
        JNIEnv *env, jclass type, jbyteArray dest_, jbyteArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memmove(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemmove__Lshort_3_093_2Lshort_3_093_2J(
        JNIEnv *env, jclass type, jshortArray dest_, jshortArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memmove(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemmove__Lint_3_093_2Lint_3_093_2J(
        JNIEnv *env, jclass type, jintArray dest_, jintArray src_, jlong count) {
    void *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    void *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memmove(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemmove__Llong_3_093_2Llong_3_093_2J(
        JNIEnv *env, jclass type, jlongArray dest_, jlongArray src_, jlong count) {
    jlong *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    jlong *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memmove(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemmove__Lfloat_3_093_2Lfloat_3_093_2J(
        JNIEnv *env, jclass type, jfloatArray dest_, jfloatArray src_, jlong count) {
    jfloat *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    jfloat *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memmove(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}

JNIEXPORT jlong JNICALL
Java_com_longlinkislong_lwjgl_system_libc_LibCString_nmemmove__Ldouble_3_093_2Ldouble_3_093_2J(
        JNIEnv *env, jclass type, jdoubleArray dest_, jdoubleArray src_, jlong count) {
    jdouble *dest = (*env)->GetPrimitiveArrayCritical(env, dest_, NULL);
    jdouble *src = (*env)->GetPrimitiveArrayCritical(env, src_, NULL);

    void *result = memmove(dest, src, (size_t) count);

    (*env)->ReleasePrimitiveArrayCritical(env, dest_, dest, 0);
    (*env)->ReleasePrimitiveArrayCritical(env, src_, src, 0);

    return (jlong) result;
}
/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 */
package com.longlinkislong.lwjgl.system;


import android.support.annotation.Nullable;
import android.util.Log;

import static com.longlinkislong.lwjgl.system.Checks.DEBUG;
import static com.longlinkislong.lwjgl.system.MathUtil.toUnsignedLong;

/**
 * Utility class useful to API bindings. [INTERNAL USE ONLY]
 *
 * <p>Method names in this class are prefixed with {@code api} to avoid ambiguities when used with static imports.</p>
 *
 */
public final class APIUtil {

    private APIUtil() {
    }

    /**
     * Prints the specified message to Debug.
     *
     * @param msg the message to print
     */
    public static void apiLog(@Nullable CharSequence msg) {
        if (DEBUG) {
            Log.d("LWJGL", msg.toString());
        }
    }

    public static long apiGetBytes(int elements, int elementShift) {
        return toUnsignedLong(elements) << elementShift;
    }

    public static void apiCheckAllocation(int elements, long bytes, long maxBytes) {
        if (!DEBUG) {
            return;
        }
        if (elements < 0) {
            throw new IllegalArgumentException("Invalid number of elements");
        }
        if ((maxBytes + Long.MIN_VALUE) < (bytes + Long.MIN_VALUE)) { // unsigned comparison
            throw new IllegalArgumentException("The request allocation is too large");
        }
    }
}
package com.longlinkislong.lwjgl.system;

import java.util.concurrent.atomic.AtomicBoolean;

public class Library {
    private Library() {}

    private static final AtomicBoolean IS_INITIALIZED = new AtomicBoolean(false);
    public static void initialize() {
        if (!IS_INITIALIZED.getAndSet(true)) {
            System.loadLibrary("lwjgl-core");
        }
    }
}

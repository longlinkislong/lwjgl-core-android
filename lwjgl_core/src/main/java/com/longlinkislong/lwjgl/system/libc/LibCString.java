/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 * MACHINE GENERATED FILE, DO NOT EDIT
 */
package com.longlinkislong.lwjgl.system.libc;

import com.longlinkislong.lwjgl.system.CustomBuffer;
import com.longlinkislong.lwjgl.system.Library;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import static com.longlinkislong.lwjgl.system.Checks.CHECKS;
import static com.longlinkislong.lwjgl.system.Checks.check;
import static com.longlinkislong.lwjgl.system.MathUtil.toUnsignedLong;
import static com.longlinkislong.lwjgl.system.MemoryUtil.memAddress;

/** Native bindings to string.h. */
public class LibCString {

    static { Library.initialize(); }

    protected LibCString() {
        throw new UnsupportedOperationException();
    }

    // --- [ memset ] ---

    /**
     * Unsafe version of: {@link #memset}
     *
     * @param count number of bytes to fill
     */
    public static native long nmemset(long dest, int c, long count);

    /**
     * Fills a memory area with a constant byte.
     *
     * @param dest pointer to the memory area to fill
     * @param c    byte to set
     *
     * @return the value of {@code dest}
     */
    public static long memset(ByteBuffer dest, int c) {
        return nmemset(memAddress(dest), c, dest.remaining());
    }

    /**
     * Fills a memory area with a constant byte.
     *
     * @param dest pointer to the memory area to fill
     * @param c    byte to set
     *
     * @return the value of {@code dest}
     */
    public static long memset(ShortBuffer dest, int c) {
        return nmemset(memAddress(dest), c, toUnsignedLong(dest.remaining()) << 1);
    }

    /**
     * Fills a memory area with a constant byte.
     *
     * @param dest pointer to the memory area to fill
     * @param c    byte to set
     *
     * @return the value of {@code dest}
     */

    public static long memset(IntBuffer dest, int c) {
        return nmemset(memAddress(dest), c, toUnsignedLong(dest.remaining()) << 2);
    }

    /**
     * Fills a memory area with a constant byte.
     *
     * @param dest pointer to the memory area to fill
     * @param c    byte to set
     *
     * @return the value of {@code dest}
     */
     public static long memset(LongBuffer dest, int c) {
        return nmemset(memAddress(dest), c, toUnsignedLong(dest.remaining()) << 3);
    }

    /**
     * Fills a memory area with a constant byte.
     *
     * @param dest pointer to the memory area to fill
     * @param c    byte to set
     *
     * @return the value of {@code dest}
     */
    public static long memset(FloatBuffer dest, int c) {
        return nmemset(memAddress(dest), c, toUnsignedLong(dest.remaining()) << 2);
    }

    /**
     * Fills a memory area with a constant byte.
     *
     * @param dest pointer to the memory area to fill
     * @param c    byte to set
     *
     * @return the value of {@code dest}
     */
    public static long memset(DoubleBuffer dest, int c) {
        return nmemset(memAddress(dest), c, toUnsignedLong(dest.remaining()) << 3);
    }

    // --- [ memcpy ] ---

    /**
     * Unsafe version of: {@link #memcpy}
     *
     * @param count the number of bytes to be copied
     */
    public static native long nmemcpy(long dest, long src, long count);

    /**
     * Copies bytes between memory areas that must not overlap.
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memcpy(ByteBuffer dest, ByteBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemcpy(memAddress(dest), memAddress(src), src.remaining());
    }

    /**
     * Copies bytes between memory areas that must not overlap.
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memcpy(ShortBuffer dest, ShortBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemcpy(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 1);
    }

    /**
     * Copies bytes between memory areas that must not overlap.
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memcpy(IntBuffer dest, IntBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemcpy(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 2);
    }

    /**
     * Copies bytes between memory areas that must not overlap.
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memcpy(LongBuffer dest, LongBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemcpy(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 3);
    }

    /**
     * Copies bytes between memory areas that must not overlap.
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memcpy(FloatBuffer dest, FloatBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemcpy(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 2);
    }

    /**
     * Copies bytes between memory areas that must not overlap.
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memcpy(DoubleBuffer dest, DoubleBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemcpy(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 3);
    }

    // --- [ memmove ] ---

    /**
     * Unsafe version of: {@link #memmove}
     *
     * @param count the number of bytes to be copied
     */
    public static native long nmemmove(long dest, long src, long count);

    /**
     * Copies {@code count} bytes from memory area {@code src} to memory area {@code dest}.
     *
     * <p>The memory areas may overlap: copying takes place as though the bytes in {@code src} are first copied into a temporary array that does not overlap
     * {@code src} or {@code dest}, and the bytes are then copied from the temporary array to {@code dest}.</p>
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memmove(ByteBuffer dest, ByteBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemmove(memAddress(dest), memAddress(src), src.remaining());
    }

    /**
     * Copies {@code count} bytes from memory area {@code src} to memory area {@code dest}.
     *
     * <p>The memory areas may overlap: copying takes place as though the bytes in {@code src} are first copied into a temporary array that does not overlap
     * {@code src} or {@code dest}, and the bytes are then copied from the temporary array to {@code dest}.</p>
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memmove(ShortBuffer dest, ShortBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemmove(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 1);
    }

    /**
     * Copies {@code count} bytes from memory area {@code src} to memory area {@code dest}.
     *
     * <p>The memory areas may overlap: copying takes place as though the bytes in {@code src} are first copied into a temporary array that does not overlap
     * {@code src} or {@code dest}, and the bytes are then copied from the temporary array to {@code dest}.</p>
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memmove(IntBuffer dest, IntBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemmove(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 2);
    }

    /**
     * Copies {@code count} bytes from memory area {@code src} to memory area {@code dest}.
     *
     * <p>The memory areas may overlap: copying takes place as though the bytes in {@code src} are first copied into a temporary array that does not overlap
     * {@code src} or {@code dest}, and the bytes are then copied from the temporary array to {@code dest}.</p>
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memmove(LongBuffer dest, LongBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemmove(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 3);
    }

    /**
     * Copies {@code count} bytes from memory area {@code src} to memory area {@code dest}.
     *
     * <p>The memory areas may overlap: copying takes place as though the bytes in {@code src} are first copied into a temporary array that does not overlap
     * {@code src} or {@code dest}, and the bytes are then copied from the temporary array to {@code dest}.</p>
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memmove(FloatBuffer dest, FloatBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemmove(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 2);
    }

    /**
     * Copies {@code count} bytes from memory area {@code src} to memory area {@code dest}.
     *
     * <p>The memory areas may overlap: copying takes place as though the bytes in {@code src} are first copied into a temporary array that does not overlap
     * {@code src} or {@code dest}, and the bytes are then copied from the temporary array to {@code dest}.</p>
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static long memmove(DoubleBuffer dest, DoubleBuffer src) {
        if (CHECKS) {
            check(dest, src.remaining());
        }
        return nmemmove(memAddress(dest), memAddress(src), toUnsignedLong(src.remaining()) << 3);
    }

    /** Array version of: {@link #nmemset} */
    public static native long nmemset(byte[] dest, int c, long count);

    /** Array version of: {@link #memset} */
    public static long memset(byte[] dest, int c) {
        return nmemset(dest, c, toUnsignedLong(dest.length) << 0);
    }

    /** Array version of: {@link #nmemset} */
    public static native long nmemset(short[] dest, int c, long count);

    /** Array version of: {@link #memset} */
    public static long memset(short[] dest, int c) {
        return nmemset(dest, c, toUnsignedLong(dest.length) << 1);
    }

    /** Array version of: {@link #nmemset} */
    public static native long nmemset(int[] dest, int c, long count);

    /** Array version of: {@link #memset} */
    public static long memset(int[] dest, int c) {
        return nmemset(dest, c, toUnsignedLong(dest.length) << 2);
    }

    /** Array version of: {@link #nmemset} */
    public static native long nmemset(long[] dest, int c, long count);

    /** Array version of: {@link #memset} */
    public static long memset(long[] dest, int c) {
        return nmemset(dest, c, toUnsignedLong(dest.length) << 3);
    }

    /** Array version of: {@link #nmemset} */
    public static native long nmemset(float[] dest, int c, long count);

    /** Array version of: {@link #memset} */
    public static long memset(float[] dest, int c) {
        return nmemset(dest, c, toUnsignedLong(dest.length) << 2);
    }

    /** Array version of: {@link #nmemset} */
    public static native long nmemset(double[] dest, int c, long count);

    /** Array version of: {@link #memset} */
    public static long memset(double[] dest, int c) {
        return nmemset(dest, c, toUnsignedLong(dest.length) << 3);
    }

    /** Array version of: {@link #nmemcpy} */
    public static native long nmemcpy(byte[] dest, byte[] src, long count);

    /** Array version of: {@link #memcpy} */
    public static long memcpy(byte[] dest, byte[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemcpy(dest, src, toUnsignedLong(src.length) << 0);
    }

    /** Array version of: {@link #nmemcpy} */
    public static native long nmemcpy(short[] dest, short[] src, long count);

    /** Array version of: {@link #memcpy} */
    public static long memcpy(short[] dest, short[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemcpy(dest, src, toUnsignedLong(src.length) << 1);
    }

    /** Array version of: {@link #nmemcpy} */
    public static native long nmemcpy(int[] dest, int[] src, long count);

    /** Array version of: {@link #memcpy} */
    public static long memcpy(int[] dest, int[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemcpy(dest, src, toUnsignedLong(src.length) << 2);
    }

    /** Array version of: {@link #nmemcpy} */
    public static native long nmemcpy(long[] dest, long[] src, long count);

    /** Array version of: {@link #memcpy} */
    public static long memcpy(long[] dest, long[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemcpy(dest, src, toUnsignedLong(src.length) << 3);
    }

    /** Array version of: {@link #nmemcpy} */
    public static native long nmemcpy(float[] dest, float[] src, long count);

    /** Array version of: {@link #memcpy} */
    public static long memcpy(float[] dest, float[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemcpy(dest, src, toUnsignedLong(src.length) << 2);
    }

    /** Array version of: {@link #nmemcpy} */
    public static native long nmemcpy(double[] dest, double[] src, long count);

    /** Array version of: {@link #memcpy} */
    public static long memcpy(double[] dest, double[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemcpy(dest, src, toUnsignedLong(src.length) << 3);
    }

    /** Array version of: {@link #nmemmove} */
    public static native long nmemmove(byte[] dest, byte[] src, long count);

    /** Array version of: {@link #memmove} */
    public static long memmove(byte[] dest, byte[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemmove(dest, src, toUnsignedLong(src.length) << 0);
    }

    /** Array version of: {@link #nmemmove} */
    public static native long nmemmove(short[] dest, short[] src, long count);

    /** Array version of: {@link #memmove} */
    public static long memmove(short[] dest, short[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemmove(dest, src, toUnsignedLong(src.length) << 1);
    }

    /** Array version of: {@link #nmemmove} */
    public static native long nmemmove(int[] dest, int[] src, long count);

    /** Array version of: {@link #memmove} */
    public static long memmove(int[] dest, int[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemmove(dest, src, toUnsignedLong(src.length) << 2);
    }

    /** Array version of: {@link #nmemmove} */
    public static native long nmemmove(long[] dest, long[] src, long count);

    /** Array version of: {@link #memmove} */
    public static long memmove(long[] dest, long[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemmove(dest, src, toUnsignedLong(src.length) << 3);
    }

    /** Array version of: {@link #nmemmove} */
    public static native long nmemmove(float[] dest, float[] src, long count);

    /** Array version of: {@link #memmove} */
    public static long memmove(float[] dest, float[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemmove(dest, src, toUnsignedLong(src.length) << 2);
    }

    /** Array version of: {@link #nmemmove} */
    public static native long nmemmove(double[] dest, double[] src, long count);

    /** Array version of: {@link #memmove} */
    public static long memmove(double[] dest,double[] src) {
        if (CHECKS) {
            check(dest, src.length);
        }
        return nmemmove(dest, src, toUnsignedLong(src.length) << 3);
    }

    /**
     * Fills memory with a constant byte.
     *
     * @param dest pointer to destination
     * @param c    character to set
     *
     * @return the value of {@code dest}
     */
    public static <T extends CustomBuffer<T>> long memset(T dest, int c) {
        return nmemset(memAddress(dest), c, toUnsignedLong(dest.remaining()) * dest.sizeof());
    }

    /**
     * Copies bytes between memory areas that must not overlap.
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static <T extends CustomBuffer<T>> long memcpy(T dest, T src) {
        if (CHECKS) {
            check(src, dest.remaining());
        }
        return nmemcpy(memAddress(dest), memAddress(src), (long)src.remaining() * src.sizeof());
    }

    /**
     * Copies {@code count} bytes from memory area {@code src} to memory area {@code dest}.
     *
     * <p>The memory areas may overlap: copying takes place as though the bytes in {@code src} are first copied into a temporary array that does not overlap
     * {@code src} or {@code dest}, and the bytes are then copied from the temporary array to {@code dest}.</p>
     *
     * @param dest pointer to the destination memory area
     * @param src  pointer to the source memory area
     *
     * @return the value of {@code dest}
     */
    public static <T extends CustomBuffer<T>> long memmove(T dest, T src) {
        if (CHECKS) {
            check(src, dest.remaining());
        }
        return nmemmove(memAddress(dest), memAddress(src), (long)src.remaining() * src.sizeof());
    }
}
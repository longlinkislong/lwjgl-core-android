/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 * MACHINE GENERATED FILE, DO NOT EDIT
 */
package com.longlinkislong.lwjgl.system.jni;

import android.support.annotation.Nullable;

import com.longlinkislong.lwjgl.system.Library;

import java.nio.Buffer;
import java.nio.ByteBuffer;

import static com.longlinkislong.lwjgl.system.Checks.CHECKS;
import static com.longlinkislong.lwjgl.system.Checks.check;

/**
 * Bindings to the Java Native Interface (JNI).
 *
 * <p>The JNI is a native programming interface. It allows Java code that runs inside a Java Virtual Machine (VM) to interoperate with applications and
 * libraries written in other programming languages, such as C, C++, and assembly.</p>
 *
 * <p>The most important benefit of the JNI is that it imposes no restrictions on the implementation of the underlying Java VM. Therefore, Java VM vendors
 * can add support for the JNI without affecting other parts of the VM. Programmers can write one version of a native application or library and expect it
 * to work with all Java VMs supporting the JNI.</p>
 *
 * <p>LWJGL: Only functions that can reasonably be called from Java are exposed.</p>
 */
public final class JNINativeInterface {
    static { Library.initialize(); }

    protected JNINativeInterface() {
        throw new UnsupportedOperationException();
    }
    // --- [ NewDirectByteBuffer ] ---

    /** Unsafe version of: {@link #NewDirectByteBuffer} */
    @Nullable
    public static native ByteBuffer nNewDirectByteBuffer(long address, long capacity);

    /**
     * Allocates and returns a direct {@code java.nio.ByteBuffer} referring to the block of memory starting at the memory address address and extending
     * capacity bytes.
     *
     * <p>Native code that calls this function and returns the resulting byte-buffer object to Java-level code should ensure that the buffer refers to a valid
     * region of memory that is accessible for reading and, if appropriate, writing. An attempt to access an invalid memory location from Java code will
     * either return an arbitrary value, have no visible effect, or cause an unspecified exception to be thrown.</p>
     *
     * @param address  the starting address of the memory region (must not be {@code NULL})
     * @param capacity the size in bytes of the memory region (must be positive)
     *
     * @return a local reference to the newly-instantiated {@code java.nio.ByteBuffer} object. Returns {@code NULL} if an exception occurs, or if JNI access to direct
     *         buffers is not supported by this virtual machine.
     */
    @Nullable
    public static ByteBuffer NewDirectByteBuffer(long address, long capacity) {
        if (CHECKS) {
            check(address);
        }

        return nNewDirectByteBuffer(address, capacity);
    }

    // --- [ GetDirectBufferAddress ] ---

    /**
     * Fetches and returns the starting address of the memory region referenced by the given direct {@code java.nio.Buffer}.
     *
     * <p>This function allows native code to access the same memory region that is accessible to Java code via the buffer object.</p>
     *
     * @param buf a direct {@code java.nio.Buffer} object (must not be {@code NULL})
     *
     * @return the starting address of the memory region referenced by the buffer. Returns {@code NULL} if the memory region is undefined, if the given object is not a
     *         direct {@code java.nio.Buffer}, or if JNI access to direct buffers is not supported by this virtual machine.
     */
    public static native long GetDirectBufferAddress(Buffer buf);
}
/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 */
package com.longlinkislong.lwjgl.system;

import static com.longlinkislong.lwjgl.system.libc.LibCStdlib.naligned_alloc;
import static com.longlinkislong.lwjgl.system.libc.LibCStdlib.naligned_free;
import static com.longlinkislong.lwjgl.system.libc.LibCStdlib.ncalloc;
import static com.longlinkislong.lwjgl.system.libc.LibCStdlib.nfree;
import static com.longlinkislong.lwjgl.system.libc.LibCStdlib.nmalloc;
import static com.longlinkislong.lwjgl.system.libc.LibCStdlib.nrealloc;

/** Provides {@link MemoryUtil.MemoryAllocator} implementations for {@link MemoryUtil} to use. */
final class MemoryManage {

    private MemoryManage() {
    }

    static MemoryUtil.MemoryAllocator getInstance() {
        return new StdlibAllocator();
    }

    /** stdlib memory allocator. */
    private static class StdlibAllocator implements MemoryUtil.MemoryAllocator {
        @Override public long malloc(long size)                        { return nmalloc(size); }
        @Override public long calloc(long num, long size)              { return ncalloc(num, size); }
        @Override public long realloc(long ptr, long size)             { return nrealloc(ptr, size); }
        @Override public void free(long ptr)                           { nfree(ptr); }
        @Override public long aligned_alloc(long alignment, long size) { return naligned_alloc(alignment, size); }
        @Override public void aligned_free(long ptr)                   { naligned_free(ptr); }

    }
}
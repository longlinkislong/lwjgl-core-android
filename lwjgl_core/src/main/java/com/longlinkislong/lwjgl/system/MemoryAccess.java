/*
 * Copyright LWJGL. All rights reserved.
 * License terms: https://www.lwjgl.org/license
 */
package com.longlinkislong.lwjgl.system;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.Objects;

import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.getByte;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.getDouble;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.getFloat;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.getInt;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.getLong;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.getShort;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.putByte;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.putDouble;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.putFloat;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.putInt;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.putLong;
import static com.longlinkislong.lwjgl.system.MemoryAccessJNI.putShort;
import static com.longlinkislong.lwjgl.system.jni.JNINativeInterface.GetDirectBufferAddress;
import static com.longlinkislong.lwjgl.system.jni.JNINativeInterface.NewDirectByteBuffer;
import static com.longlinkislong.lwjgl.system.libc.LibCString.nmemcpy;
import static com.longlinkislong.lwjgl.system.libc.LibCString.nmemset;

/** Provides {@link MemoryAccessor} implementations. The most efficient available will be used by {@link MemoryUtil}. */
final class MemoryAccess {
    private MemoryAccess() {
    }

    static MemoryAccessor getInstance() {
        return new MemoryAccessorJNI();
    }

    /** Implements functionality for {@link MemoryUtil}. */
    interface MemoryAccessor {

        int getPageSize();

        int getCacheLineSize();

        long memAddress0(Buffer buffer);

        ByteBuffer memByteBuffer(long address, int capacity);

        ShortBuffer memShortBuffer(long address, int capacity);
        CharBuffer memCharBuffer(long address, int capacity);
        IntBuffer memIntBuffer(long address, int capacity);
        LongBuffer memLongBuffer(long address, int capacity);
        FloatBuffer memFloatBuffer(long address, int capacity);
        DoubleBuffer memDoubleBuffer(long address, int capacity);

        void memSet(long dst, int value, long bytes);

        void memCopy(long src, long dst, long bytes);

        byte memGetByte(long ptr);
        short memGetShort(long ptr);
        int memGetInt(long ptr);
        long memGetLong(long ptr);
        float memGetFloat(long ptr);
        double memGetDouble(long ptr);

        void memPutByte(long ptr, byte value);
        void memPutShort(long ptr, short value);
        void memPutInt(long ptr, int value);
        void memPutLong(long ptr, long value);
        void memPutFloat(long ptr, float value);
        void memPutDouble(long ptr, double value);

        MemoryTextUtil getTextUtil();

    }

    private static final class MemoryAccessorJNI implements MemoryAccessor {
        @Override
        public int getPageSize() {
            return 4096;
        }

        @Override
        public int getCacheLineSize() {
            return 64;
        }

        @Override
        public long memAddress0(Buffer buffer) {
            return GetDirectBufferAddress(buffer);
        }

        @Override
        public ByteBuffer memByteBuffer(long address, int capacity) {
            return Objects.requireNonNull(NewDirectByteBuffer(address, capacity)).order(ByteOrder.nativeOrder());
        }

        @Override
        public ShortBuffer memShortBuffer(long address, int capacity) {
            return memByteBuffer(address, capacity << 1).asShortBuffer();
        }

        @Override
        public CharBuffer memCharBuffer(long address, int capacity) {
            return memByteBuffer(address, capacity << 1).asCharBuffer();
        }

        @Override
        public IntBuffer memIntBuffer(long address, int capacity) {
            return memByteBuffer(address, capacity << 2).asIntBuffer();
        }

        @Override
        public LongBuffer memLongBuffer(long address, int capacity) {
            return memByteBuffer(address, capacity << 3).asLongBuffer();
        }

        @Override
        public FloatBuffer memFloatBuffer(long address, int capacity) {
            return memByteBuffer(address, capacity << 2).asFloatBuffer();
        }

        @Override
        public DoubleBuffer memDoubleBuffer(long address, int capacity) {
            return memByteBuffer(address, capacity << 3).asDoubleBuffer();
        }

        @Override
        public void memSet(long dst, int value, long bytes) {
            nmemset(dst, value, bytes);
        }

        @Override
        public void memCopy(long src, long dst, long bytes) {
            nmemcpy(src, dst, bytes);
        }

        @Override
        public byte memGetByte(long ptr) {
            return getByte(ptr);
        }

        @Override
        public short memGetShort(long ptr) {
            return getShort(ptr);
        }

        @Override
        public int memGetInt(long ptr) {
            return getInt(ptr);
        }

        @Override
        public long memGetLong(long ptr) {
            return getLong(ptr);
        }

        @Override
        public float memGetFloat(long ptr) {
            return getFloat(ptr);
        }

        @Override
        public double memGetDouble(long ptr) {
            return getDouble(ptr);
        }

        @Override
        public void memPutByte(long ptr, byte value) {
            putByte(ptr, value);
        }

        @Override
        public void memPutShort(long ptr, short value) {
            putShort(ptr, value);
        }

        @Override
        public void memPutInt(long ptr, int value) {
            putInt(ptr, value);
        }

        @Override
        public void memPutLong(long ptr, long value) {
            putLong(ptr, value);
        }

        @Override
        public void memPutFloat(long ptr, float value) {
            putFloat(ptr, value);
        }

        @Override
        public void memPutDouble(long ptr, double value) {
            putDouble(ptr, value);
        }

        @Override
        public MemoryTextUtil getTextUtil() {
            return new MemoryTextUtil();
        }
    }
}
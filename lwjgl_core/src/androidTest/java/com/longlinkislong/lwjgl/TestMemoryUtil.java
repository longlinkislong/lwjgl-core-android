package com.longlinkislong.lwjgl;

import android.support.test.runner.AndroidJUnit4;

import com.longlinkislong.lwjgl.system.MemoryUtil;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.ByteBuffer;

@RunWith(AndroidJUnit4.class)
public class TestMemoryUtil {
    @Test
    public void testMalloc() {
        ByteBuffer ptr = MemoryUtil.memAlloc(1024);

        Assert.assertEquals(1024, ptr.capacity());
        Assert.assertNotEquals(MemoryUtil.NULL, MemoryUtil.memAddress(ptr));

        MemoryUtil.memFree(ptr);
    }

    @Test
    public void testHeap() {
        final int size = 1024 * 1024;

        ByteBuffer ptr = MemoryUtil.memAlloc(size);

        Assert.assertEquals(size, ptr.capacity());
        Assert.assertNotEquals(MemoryUtil.NULL, MemoryUtil.memAddress(ptr));

        while (ptr.hasRemaining()) {
            ptr.putInt(0xBEEFD00D);
        }

        ptr.flip();

        while (ptr.hasRemaining()) {
            Assert.assertEquals(0xBEEFD00D, ptr.getInt());
        }

        MemoryUtil.memFree(ptr);
    }

    @Test
    public void testMemSet() {
        ByteBuffer ptr = MemoryUtil.memAlloc(1024);

        Assert.assertEquals(1024, ptr.capacity());
        Assert.assertNotEquals(MemoryUtil.NULL, MemoryUtil.memAddress(ptr));

        MemoryUtil.memSet(ptr, 0);

        while (ptr.hasRemaining()) {
            Assert.assertEquals(0, ptr.get());
        }

        MemoryUtil.memFree(ptr);
    }
}

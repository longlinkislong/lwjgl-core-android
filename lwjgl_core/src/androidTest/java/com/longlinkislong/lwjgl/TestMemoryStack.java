package com.longlinkislong.lwjgl;

import android.support.test.runner.AndroidJUnit4;

import com.longlinkislong.lwjgl.system.MemoryStack;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

@RunWith(AndroidJUnit4.class)
public class TestMemoryStack {
    @Test
    public void testMemoryStack() {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pI = mem.ints(10, 20, 30, 40);
            final FloatBuffer pF = mem.floats(1.1F ,2.2F, 3.3F, 4.4F);

            Assert.assertEquals(10, pI.get(0));
            Assert.assertEquals(20, pI.get(1));
            Assert.assertEquals(30, pI.get(2));
            Assert.assertEquals(40, pI.get(3));

            Assert.assertEquals(1.1F, pF.get(0), 1E-7F);
            Assert.assertEquals(2.2F, pF.get(1), 1E-7F);
            Assert.assertEquals(3.3F, pF.get(2), 1E-7F);
            Assert.assertEquals(4.4F, pF.get(3), 1E-7F);
        }
    }
}

# LWJGL core for Android
A minimal port of LWJGL 3 Core module for Android 4.4+

Exposes MemoryUtil, MemoryStack, BufferUtils, and PointerBuffer.

# Changes from LWJGL 3
* Only MemoryAccessJNI is implemented
* Some very minor workarounds to work on Java7